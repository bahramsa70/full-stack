require("dotenv").config();
const fs = require("fs");
const { createServer } = require("https");
const { app } = require("./src/app");

const port = process.env.PORT;

const server = createServer(
  {
    key: fs.readFileSync("key.pem"),
    cert: fs.readFileSync("cert.pem"),
  },
  app
);

server.listen(port);

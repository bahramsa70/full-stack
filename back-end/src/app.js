const cors = require("cors");
const express = require("express");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const { notesRouter } = require("./routers/notes.routes");
const { authRouter } = require("./routers/auth.routes");

mongoose.connect("mongodb://localhost:27017/full-stack-db");

const app = express();
app.use(express.json());
app.use(cors());

const authValidation = (req, res, next) => {
  const token = req.headers.authorization?.replace("Bearer ", "");

  if (token) {
    try {
      jwt.verify(token, process.env.JWT_PRIVATE_KEY);
    } catch (error) {
      return res.status(403).send("Unauthorized");
    }
  } else {
    return res.status(401).send("Unauthorized");
  }

  next();
};

app.use("/notes", authValidation, notesRouter);
app.use("/", authRouter);

module.exports = {
  app,
  mongoose,
};

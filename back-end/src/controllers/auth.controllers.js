const jwt = require("jsonwebtoken");
const { authModel } = require("../models/auth.model");

const signup = async (req, res) => {
  const payload = req.body;
  const isRegistered = await authModel.findOne({ username: payload.username });

  if (isRegistered) {
    return res.json({ valid: false });
  }

  await authModel.create(payload);
  return res.json({ valid: true });
};

const signin = async (req, res) => {
  const payload = req.body;
  const user = await authModel.findOne({ username: payload.username });

  if (user) {
    if (user.password === payload.password) {
      const newJwt = jwt.sign(
        { username: payload.username },
        process.env.JWT_PRIVATE_KEY,
        { expiresIn: "1h" }
      );

      return res.json({ valid: true, jwt: newJwt });
    }

    return res.json({ valid: false, message: "Password is wrong!" });
  }

  return res.json({ valid: false, message: "Username not found!" });
};

module.exports = { signup, signin };

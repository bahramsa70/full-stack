const { notesModel } = require("../models/notes.model");

const notesGetList = async (_, res) => {
  const notes = await notesModel.find({});
  return res.json(notes);
};

const newNote = async (req, res) => {
  const payload = req.body;
  const newNote = await notesModel.create(payload);
  return res.status(200).json({ ...payload, id: newNote.id });
};

const deleteNote = async (req, res) => {
  const id = req.query?.id;
  await notesModel.findByIdAndDelete(id);
  return res.status(200).json(id);
};

const editNote = async (req, res) => {
  const payload = req.body;
  await notesModel.findByIdAndUpdate(payload.id, payload);
  return res.status(200).json({
    title: payload.title,
    description: payload.description,
    _id: payload.id,
  });
};

module.exports = { notesGetList, newNote, deleteNote, editNote };

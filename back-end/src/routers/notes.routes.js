const express = require("express");
const {
  notesGetList,
  newNote,
  deleteNote,
  editNote,
} = require("../controllers/notes.controllers");

const notesRouter = express.Router();
notesRouter.get("/", notesGetList);
notesRouter.post("/new", newNote);
notesRouter.delete("/delete", deleteNote);
notesRouter.put("/edit", editNote);

module.exports = {
  notesRouter,
};

import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import Signup from "../pages/Signup";
import Signin from "../pages/Signin";
import Notes from "../pages/Notes/index";

const AppRoutes = () => {
  return (
    <Routes>
      <Route element={<SignupProtector />}>
        <Route path="/" element={<Signin />} />
      </Route>

      <Route element={<SignupProtector />}>
        <Route path="/signup" element={<Signup />} />
      </Route>

      <Route element={<NotesProtector />}>
        <Route path="/notes" element={<Notes />} />
      </Route>
    </Routes>
  );
};

export default AppRoutes;

const SignupProtector = () => {
  const jwt = localStorage.getItem("jwt");

  if (jwt) {
    return <Navigate to="/notes" />;
  }

  return <Outlet />;
};

const NotesProtector = () => {
  const jwt = localStorage.getItem("jwt");

  if (jwt) {
    return <Outlet />;
  }

  return <Navigate to="/" />;
};

import axios from "axios";

const instance = axios.create({
  baseURL: "https://localhost:4001",
  headers: {
    "Content-Type": "application/json",
  },
});

const token = () => {
  const jwt = localStorage.getItem("jwt");
  return jwt && `Bearer ${jwt}`;
};

instance.interceptors.request.use(
  (req) => {
    req.headers.Authorization = token();
    return req;
  },
  (err) => {
    return Promise.reject(err);
  }
);

instance.interceptors.response.use(
  (res) => {
    return res;
  },
  (err) => {
    if (err?.response?.status === 401 || err?.response?.status === 403) {
      localStorage.clear();
      window.location.replace("/");
    }
  }
);

export { instance };

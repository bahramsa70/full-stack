import { useState } from "react";

// eslint-disable-next-line react/prop-types
const Note = ({ title, description, id, editHandler, deleteHandler }) => {
  const [values, setValues] = useState({ title, description, id });

  return (
    <div
      style={{
        display: "flex",
        gap: "20px",
        padding: "20px",
        width: "80vw",
        border: "1px solid red",
      }}
    >
      <label>
        Title
        <input
          type="text"
          value={values.title}
          onChange={(e) => setValues({ ...values, title: e.target.value })}
        />
      </label>

      <label>
        Description
        <input
          type="text"
          value={values.description}
          onChange={(e) =>
            setValues({ ...values, description: e.target.value })
          }
        />
      </label>

      <button onClick={() => editHandler({ ...values, id })}>Edit</button>

      <button onClick={() => deleteHandler(id)}>Delete</button>
    </div>
  );
};

export default Note;

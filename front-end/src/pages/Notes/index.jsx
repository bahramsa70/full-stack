import { useEffect, useState } from "react";
import { instance } from "../../instance";
import Note from "./Note";

const Notes = () => {
  const [list, setList] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const req = await instance.get("/notes");
      const res = await req.data;
      setList(res);
    };

    getData();
  }, []);

  const [values, setValues] = useState({ title: "", description: "" });

  const newHandler = async () => {
    const req = await instance.post("/notes/new", values);
    const res = await req.data;
    setList([...list, res]);
    setValues({ title: "", description: "" });
  };

  const editHandler = async (v) => {
    const req = await instance.put("/notes/edit", v);
    const res = await req.data;
    const currentData = [...list];
    const dataIndex = currentData.findIndex((item) => item._id === res._id);
    currentData[dataIndex] = res;
    setList(currentData);
  };

  const deleteHandler = async (id) => {
    const req = await instance.delete(`/notes/delete?id=${id}`);
    const deleteId = await req.data;
    const newList = list.filter((x) => x._id !== deleteId);
    setList(newList);
  };

  return (
    <div
      style={{
        padding: "30px",
        display: "flex",
        flexDirection: "column",
        gap: "20px",
      }}
    >
      <h1>Notes</h1>

      <div style={{ padding: "30px", display: "flex", gap: "30px" }}>
        <label>
          Title
          <input
            type="text"
            value={values.title}
            onChange={(e) => setValues({ ...values, title: e.target.value })}
          />
        </label>

        <label>
          Description
          <input
            type="text"
            value={values.description}
            onChange={(e) =>
              setValues({ ...values, description: e.target.value })
            }
          />
        </label>

        <button onClick={newHandler}>Add</button>
      </div>

      {list.map((item) => (
        <Note
          id={item._id}
          title={item.title}
          description={item.description}
          deleteHandler={deleteHandler}
          editHandler={editHandler}
          key={item._id}
        />
      ))}
    </div>
  );
};

export default Notes;

import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { instance } from "../instance";

const Signin = () => {
  const [values, setValues] = useState({ username: "", password: "" });
  const navigate = useNavigate();

  const submitHandler = async () => {
    const req = await instance.post("/signin", values);
    const res = await req.data;

    if (res.valid) {
      localStorage.setItem("jwt", res.jwt);
      navigate("/notes");
    } else {
      alert(res.message);
    }
  };

  return (
    <div style={{ padding: "30px" }}>
      <h1>Signin</h1>

      <div style={{ padding: "10px 0 30px 0" }}>
        <a href="/signup">Signup</a>
      </div>

      <div style={{ display: "flex", gap: "30px" }}>
        <label>
          username
          <input
            type="text"
            value={values.username}
            onChange={(e) => setValues({ ...values, username: e.target.value })}
          />
        </label>

        <label>
          password
          <input
            type="text"
            value={values.password}
            onChange={(e) => setValues({ ...values, password: e.target.value })}
          />
        </label>

        <button onClick={submitHandler}>Sign in</button>
      </div>
    </div>
  );
};

export default Signin;

import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { instance } from "../instance";

const Signup = () => {
  const [values, setValues] = useState({ username: "", password: "" });
  const navigate = useNavigate();

  const submitHandler = async () => {
    const req = await instance.post("/signup", values);
    const { valid } = await req.data;

    if (valid) {
      navigate("/");
    } else {
      alert("The username is already registered!");
    }
  };

  return (
    <div style={{ padding: "30px" }}>
      <h1>Signup</h1>

      <div style={{ padding: "10px 0 30px 0" }}>
        <a href="/">Signin</a>
      </div>

      <div style={{ display: "flex", gap: "30px" }}>
        <label>
          username
          <input
            type="text"
            value={values.username}
            onChange={(e) => setValues({ ...values, username: e.target.value })}
          />
        </label>

        <label>
          password
          <input
            type="text"
            value={values.password}
            onChange={(e) => setValues({ ...values, password: e.target.value })}
          />
        </label>

        <button onClick={submitHandler}>Sign up</button>
      </div>
    </div>
  );
};

export default Signup;
